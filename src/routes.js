"use strict";

const express = require("express");
const config = require("./config");
const moment = require("moment");
moment.locale(config.locale);
const db = require("./datastore");
const categories = require("./categories");
const router = express.Router();
const publish = require("./publish");
const i18n = require("./i18n");
const pug = require("pug");

//
// pug view setup
//

require("pug").filters = {
  "legal-vars": function(text) {
    return text
      .replace(/{{LEGAL_DOMAIN}}/g, config.legal.domain)
      .replace(/{{LEGAL_NAME}}/g, config.legal.name)
      .replace(/{{LEGAL_URL}}/g, config.legal.url);
  }
};

function templateContext(obj) {
  return Object.assign(
    {
      i18n: i18n,
      moment: moment,
      config: config,
      topItems: db.getTop().slice(0, config.topItems),
      categories: categories.sorted
    },
    obj
  );
}

//
// Routes
//

router.get(["/", "/index.html", "/page-:page.html"], (req, res) => {
  const page = parseInt(req.params.page || "0");
  const items = db
    .getFeed()
    .slice(
      config.itemsPerPage * page,
      config.itemsPerPage * page + config.itemsPerPage + 1
    );
  const hasNext = items.length > config.itemsPerPage;
  const hasPrev = req.params.page > 0;
  res.render(
    "index",
    templateContext({
      items: items.slice(0, config.itemsPerPage),
      page: page,
      hasNext: hasNext,
      hasPrev: hasPrev
    })
  );
});

router.get("/item/:itemId.html", (req, res) => {
  var item = db.getItem(req.params.itemId);
  if (item == null) return res.status(404).send("Not found");
  res.render(
    "item",
    templateContext({
      item: db.getItem(req.params.itemId)
    })
  );
});

router.get(
  ["/category/:category.html", "/category/:category/:page.html"],
  (req, res) => {
    const page = parseInt(req.params.page || "0");
    var category = categories.find(req.params.category);
    if (category == null) return res.status(404).send("Not found");
    const items = db
      .getFeed()
      .filter(category.match)
      .slice(
        config.itemsPerPage * page,
        config.itemsPerPage * page + config.itemsPerPage + 1
      );
    const hasNext = items.length > config.itemsPerPage;
    const hasPrev = req.params.page > 0;
    res.render(
      "category",
      templateContext({
        category: category,
        items: items.slice(0, config.itemsPerPage),
        page: page,
        hasNext: hasNext,
        hasPrev: hasPrev
      })
    );
  }
);

router.get("/info/:name.html", (req, res) => {
  res.render(`info/${req.params.name}`, templateContext({}));
});

// SEO

router.get("/robots.txt", (req, res) => {
  res.end(
    `
Sitemap: ${config.url}/sitemap.xml
User-agent: *
`.trim()
  );
});

router.get("/sitemap.xml", (req, res) => {
  res.set("Content-Type", "text/xml; charset=utf-8");
  res.render(
    "sitemap",
    templateContext({
      items: db.getFeed()
    })
  );
});

// OneSignal

router.get("/manifest.json", (req, res) => {
  res.json({
    name: config.siteName,
    short_name: config.domain,
    start_url: "/",
    display: "standalone",
    gcm_sender_id: "482941778795",
    gcm_sender_id_comment: "Do not change the GCM Sender ID"
  });
});

// RSS feed

router.get(["/feed.rss"], (req, res) => {
  res.set("Content-Type", "text/xml; charset=utf-8");
  res.render(
    "rss",
    templateContext({
      items: db.getFeed().slice(0, 20)
    })
  );
});

// Facebook instant Articles

router.get(["/fb/ia.rss"], (req, res) => {
  res.set("Content-Type", "text/xml; charset=utf-8");
  res.render(
    "fb/instant-articles-rss",
    templateContext({
      items: db.getFeed().slice(0, 20),
      renderInstantArticle: function(item) {
        return pug.renderFile(
          "views/fb/instant-article.pug",
          templateContext({
            iframeBaseUrl: config.url,
            pretty: true,
            item: item
          })
        );
      }
    })
  );
});

router.get(["/fb/ia-dev.rss"], (req, res) => {
  res.set("Content-Type", "text/xml; charset=utf-8");
  res.render(
    "fb/instant-articles-rss",
    templateContext({
      iframeBaseUrl: config.stagingUrl,
      items: db.getFeed().slice(0, 20),
      renderInstantArticle: function(item) {
        return pug.renderFile(
          "views/fb/instant-article.pug",
          templateContext({
            iframeBaseUrl: config.stagingUrl,
            pretty: true,
            item: item
          })
        );
      }
    })
  );
});

router.get("/fb/:itemId.html", (req, res) => {
  var item = db.getItem(req.params.itemId);
  if (item == null) return res.status(404).send("Not found");
  res.render(
    "fb/instant-article-iframe",
    templateContext({
      item: db.getItem(req.params.itemId)
    })
  );
});

// ADMIN

const auth = require("basic-auth");
router.all("/admin/*", (req, res, next) => {
  var user = auth(req);
  if (
    user === undefined ||
    user["name"] !== "admin" ||
    user["pass"] !== config.adminPassword
  ) {
    res.statusCode = 401;
    res.setHeader("WWW-Authenticate", `Basic realm="${config.siteName} admin"`);
    res.end("Unauthorized");
  } else {
    next();
  }
});

router.get("/admin/refresh", (req, res) => {
  db
    .refresh()
    .then(() => db.refreshTop())
    .then(() => res.end("OK"))
    .catch(err => res.end(JSON.stringify(err)));
});

router.get("/admin/publish", (req, res) => {
  publish().catch(err => console.log(err));
  res.end("Publishing started");
});

module.exports = router;
