# TERMS & CONDITIONS

Gentile cliente, desideriamo informarLa che il D.lgs. n. 196 del 30 giugno 2003 ("Codice in materia di protezione dei dati personali") e successive modifiche ed integrazioni prevede la tutela delle persone e di altri soggetti rispetto al trattamento dei dati personali. Secondo la normativa indicata, tale trattamento sarà improntato ai principi di correttezza, liceità e trasparenza e di tutela della Sua riservatezza e dei Suoi diritti. Ai sensi dell'articolo 13 del D.lgs. n.196/2003, pertanto, Le forniamo le seguenti informazioni:

## Finalità del trattamento cui sono destinati i dati

I dati raccolti sono finalizzati all'espletamento dei seguenti trattamenti: 
• Invio di inviti ad eventi, iniziative, promozioni commerciali e altre attività di marketing di {{LEGAL_NAME}}.

## Modalità del trattamento dei dati

I dati raccolti verranno registrati e conservati su supporti elettronici protetti e trattati con adeguate misure di sicurezza, anche associandoli ed integrandoli con altri DataBase. I dati verranno trattati esclusivamente con modalità e procedure necessarie ed idonee a fornirLe i servizi indicati al punto che precede. I dati potrebbero altresì essere utilizzati per l'accertamento di responsabilità in caso di ipotetici reati informatici ai danni del sito.

## Natura del conferimento dei dati e conseguenze di un eventuale rifiuto a rispondere

Il conferimento dell'indirizzo di posta elettronica è obbligatorio e necessario per fornirle i servizi più sopra indicati. Il mancato conferimento del medesimo comporta, quindi, l'impossibilità di erogazione del servizio. L'eventuale richiesta di altri dati è facoltativa.

##  Ambito di comunicazione e diffusione dei dati

I dati da Lei forniti non saranno diffusi a terzi diversi dal titolare e dal responsabile del trattamento (v.infra), ma potranno essere comunicati, ove necessario per l'erogazione dei servizi, a soggetti che svolgono per conto ed incarico dei soggetti suindicati attività e compiti di natura tecnica ed organizzativa strumentali alla fornitura dei servizi richiesti, sotto la diretta autorità del titolare o del responsabile, attendendosi alle istruzioni impartite.

## Estremi identificativi del titolare e del responsabile

Il Titolare e responsabile del trattamento dei dati è {{LEGAL_NAME}}.

La Società incaricata da {{LEGAL_NAME}} per l'hosting dei dati è {{LEGAL_NAME}}, contattabile all'indirizzo info@{{LEGAL_DOMAIN}}.


## Diritti di cui all'art. 7 del D.Lgs n. 196/2003

In ogni momento potrà esercitare i Suoi diritti nei confronti del titolare del trattamento, ai sensi dell'art.7 del D.lgs.196/2003, che per Sua comodità riproduciamo integralmente: 


    Decreto Legislativo 30 giugno 2003, n. 196

    Codice in materia di protezione dei dati personali

    Art. 7. Diritto di accesso ai dati personali ed altri diritti

    1. L'interessato ha diritto di ottenere la conferma dell'esistenza o meno di dati personali che lo riguardano, anche se non ancora registrati, e la loro comunicazione in forma intelligibile. 

    2. L'interessato ha diritto di ottenere l'indicazione:

    a) dell'origine dei dati personali;

    b) delle finalità e modalità del trattamento;

    c) della logica applicata in caso di trattamento effettuato con l'ausilio di strumenti elettronici;

    d) degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai sensi dell'articolo 5, comma 2;

    e) dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o incaricati.

    3. L'interessato ha diritto di ottenere:

    a) l'aggiornamento, la rettificazione ovvero, quando vi ha interesse, l'integrazione dei dati;

    b) la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati;

    c) l'attestazione che le operazioni di cui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato.

    4. L'interessato ha diritto di opporsi, in tutto o in parte:

    a) per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorchè pertinenti allo scopo della raccolta;

    b) al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale.

    Il sottoscritto interessato, accettando la presente, dichiara di essere stato preventivamente informato dal titolare circa:

    a) Le finalità e le modalità del trattamento cui sono destinati i dati;

    b) La natura obbligatoria o facoltativa del conferimento dei dati;

    c) Le conseguenze i un eventuale rifiuto di rispondere;

    d) I soggetti o le categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di responsabili od incaricati, e l'ambito di diffusione dei dati medesimi;

    e) I diritti di cui all'art. 7 del D.Lgs n. 196/2003;

    f) Gli estremi identificativi del titolare e del responsabile.

    Il sottoscritto interessato, accettando la presente, attesta il proprio libero consenso acciocchè il titolare proceda ai trattamenti di propri dati personali, nonchè alla loro comunicazione, nell'ambito dei soggetti espressamente risultanti alla voce "estremi identificativi del Titolare e del Responsabile". Prende altresì atto che l'eventuale esistenza di dati sensibili fra quelli raccolti è indicata in modo chiaro e, conscio di ciò, estende il proprio consenso anche al trattamento ed alla comunicazione di tali dati vincolandolo comunque al rispetto di ogni altra condizione imposta per legge.
