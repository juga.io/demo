# demo.juga.io website

## About

The *Demo Project* is a small but complete nodejs application which uses the Juga.io platform as a CMS.

The content is edited on [Juga.io](http://juga.io) and scheduled into a `feed`, this project reads the Juga.io API to populate its pages.

This project provides integration with the following third party services:

- **Google Analytics** to build a ranking of top items
- **OneSignal** to provide web notifications
- **Addthis** for social sharing
- **SEO** with robots.txt and automatic sitemap.xml generation
- **Amazon S3** high availability hosting since a static version of the entire website is published there at will.

You are free to use this project to bootstrap your own Juga.io website.

**Resource:** [Link to the Juga.io API documentation](https://juga.io/developers)

## How to develop

Git and npm are required to start this project.

From the terminal run the following commands:

    git clone <this repository>
    cd demo
    git checkout develop
    yarn install
    npm install -g node-dev
    yarn dev
    open http://localhost:5000

## Project structure

This is a nodejs express application with the following structure:

    public/  # the express serve the content of public statically
    views/   # where pug templates resides
    sass/    # bootstrap sass customization
    bin/     # start scripts
    src/     # javascript sources
    tests/   # jest tests

### Javascript modules

    src/
      config.js      # website configuration
      routes.js      # express routes
      app.js         # setup express and publish job
      categories.js  # list of website categories
      datastore.js   # stores juga.io feed content and top entries
      i18n.js        # translation
      jugaio-api.js  # provides convenient method to read juga.io feed
      publish.js     # scrap website and publish to amazon s3
      top.js         # use googleapis to find most visited items

### Configuring

Refer to [src/config.js](src/config.js).

### Publishing to S3

The Demo Project uses `httrack` to scrap its content and then sends it to Amazon S3 using `aws` tools.

Refer to [src/publish.js](src/publish.js).

### Top content

The Demo Project is linked to Google Analytics and uses its API to get a report of most visited items.

Refer to [src/top.js](src/top.js).

## Tasks

Things the maintainer of the project have to do:

- more documentation to come
- externalize onesignal from `view/layout.pug`
- cookie, privacy, terms are in italian, clear the .md files and put some english text instead
