/**
 * Content data-store.
 * @module datastore
 */

const config = require("./config")
const top = require("./top")
const db = {
    initialized: false,
    entries: [],
    topEntries: [],
    api: require("./jugaio-api")(config.jugaIo)
}

/**
 * Refresh items calling juga.io's API.
 * @return {Promise.<bool>}
 * @static
 */
function refresh(){
    return new Promise((fulfill, reject) => {
        db.api.readFeed(config.jugaIo.feedId, (err, data) => {
            if (err != null)
                return reject(err)
            console.log("Datastore refreshed:", data.length, "items")
            db.entries = data
            fulfill(true)
        })
    })
}

/**
 * Returns entry by id.
 * @param {string} id - The entry's ID.
 * @static
 * @return {FeedEntry|null}
 */
function getItem(id){
    return db.entries.find(e => e.itemId == id)
}

/**
 * Returns entries in the feed sorted from most recent to older.
 * @static
 * @return {Array.<FeedEntry>}
 */
function getFeed(){
    return db.entries
}

/**
 * Returns most visited entries.
 * @return {Array.<FeedEntry>}
 * @static
 */
function getTop(){
    return db.topEntries
}

/* Refresh top items. */
function refreshTop(){
    console.log("Refreshing top entries")
    return top.getTopItemIds().
        then((ids) => {
            if (ids.length == 0)
                db.topEntries = db.entries.slice(0, 10)
            else
                db.topEntries = ids.map(getItem).filter(o => o != null)
            console.log("Top entries refreshed", db.topEntries.length)
        }).
        catch(console.log)
}

/**
 * Initialize datastore.
 * @static
 */
function init(){
    if (db.initialized)
        return
    console.log("Initializing datastore refresh job")
    db.initialized = true
    refresh().then(refreshTop)
    setInterval(refresh, config.jugaIo.refreshInterval)
    setInterval(refreshTop, config.refreshTopInterval)
}

module.exports.init = init
module.exports.refresh = refresh
module.exports.getItem = getItem
module.exports.getFeed = getFeed
module.exports.getTop = getTop
