const config = require("./config")
const { execFile } = require('child_process')

function scrap(){
    return new Promise((fulfill, reject) => {
        const child = execFile("httrack", [
            `${config.stagingUrl}`,
            `${config.stagingUrl}/css/style.css`,
            `${config.stagingUrl}/app.js`,
            `${config.stagingUrl}/sitemap.xml`,
            `${config.stagingUrl}/robots.txt`,
            `${config.stagingUrl}/archives.html`,
            "-O",
            "httrack/",
            "--preserve",
            "--footer",
            "<!--jj-->"
        ], (error, stdout, stderr) => {
            console.log("Site Scrapping completed")
            if (error)
                reject(error)
            else
                fulfill(stdout)
        })
    })
}

function sync(){
    return new Promise((fulfill, reject) => {
        const domain = config.stagingUrl.replace("http://", "").replace("https://", "")
        const child = execFile("aws", [
            `--region=${config.s3Region}`,
            "s3",
            "sync",
            (config.production ? "" : "--dryrun"),
            "--acl", "public-read",
            "--cache-control", `max-age=${config.s3MaxAge}`,
            "httrack/"+domain,
            `s3://${config.s3Bucket}`
        ].filter(f => f != ""), (error, stdout, stderr) => {
            console.log("Sync completed")
            console.log(error)
            console.log(stdout)
            console.log(stderr)
            if (error)
                reject(error)
            else
                fulfill(stdout)
        })
    })
}

async function publish(){
    await scrap()
    await sync()
    console.log("Publishing complete")
}

module.exports = publish
