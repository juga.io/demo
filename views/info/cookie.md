# Cookie

## INFORMATIVA ESTESA SUI COOKIE

I Cookie sono costituiti da porzioni di codice installate all'interno del browser dell’utente che assistono il Titolare nell’erogazione del servizio in base alle finalità descritte. Alcune delle finalità di installazione dei Cookie potrebbero, inoltre, necessitare del consenso dell'Utente.

## COOKIE TECNICI E DI STATISTICA AGGREGATA

### Attività strettamente necessarie al funzionamento

Questo sito utilizza Cookie per salvare la sessione dell'utente e per svolgere altre attività strettamente necessarie al funzionamento dello stesso, ad esempio in relazione alla distribuzione del traffico.

### Attività di salvataggio delle preferenze, ottimizzazione e statistica

Questo sito utilizza Cookie per salvare le preferenze di navigazione ed ottimizzare l'esperienza di navigazione dell'Utente. Fra questi Cookie rientrano, ad esempio, quelli impiegati per impostare la lingua e la valuta o per la gestione di statistiche da parte del titolare del sito.

## ALTRE TIPOLOGIE DI COOKIE O STRUMENTI TERZI CHE POTREBBERO FARNE UTILIZZO

Alcuni dei servizi elencati di seguito raccolgono statistiche in forma aggregata e potrebbero non richiedere il consenso dell'Utente o potrebbero essere gestiti direttamente dal titolare, a seconda di quanto descritto, senza l'ausilio di terzi.

### Statistica

I servizi contenuti nella presente sezione permettono al Titolare del Trattamento di monitorare e analizzare i dati di traffico e servono a tener traccia del comportamento dell’Utente.

### Google Analytics (Google)

Google Analytics è un servizio di analisi web fornito da Google Inc. (“Google”). Google utilizza i Dati Personali raccolti allo scopo di tracciare ed esaminare l’utilizzo di questa Applicazione, compilare report e condividerli con gli altri servizi sviluppati da Google.

Google potrebbe utilizzare i Dati Personali per contestualizzare e personalizzare gli annunci del proprio network pubblicitario.

I dati personali raccolti sono: Cookie e Dati di utilizzo.

Luogo del trattamento : U.S.A.

Ai seguenti link è possibile visionare Privacy Policy  ed  Opt Out

### Social Network

Sulle pagine del Sito sono presenti pulsanti e widget di Social Network per agevolare l’interazione con le piattaforme Social e la condivisione di contenuti direttamente dalle pagine di questo sito web. A titolo di maggiore informazione, si riportano di seguito gli indirizzi web delle informative e delle modalità per la gestione dei cookie dei diversi social network:

- Facebook
- Twitter
- AddThis

## SERVIZI DI TERZE PARTI

Il sito integra, all'interno delle proprie pagine, servizi di terze parti che potrebbero impostare e utilizzare propri cookie e/o tecnologie similari. L'impiego di tali cookie e tecnologie similari da parte di tali aziende è regolato dalle informative sulla privacy di dette società e non dalla presente informativa essendo il Titolare del trattamento totalmente estraneo alla gestione di tali strumenti ed al trattamento dei dati da questi derivanti.

Forniamo di seguito un elenco (non esaustivo) di alcune delle società partner che potrebbero utilizzare i cookie mentre navighi sul network della nostra organizzazione:

- Google Analytics ([informativa](https://www.google.com/analytics/learn/privacy.html?hl=ihttps://www.google.com/analytics/learn/privacy.html?hl))
- Google Adsense ([informativa](www.google.com/policies/technologies/ads/))
- Google ([informativa](www.google.com/intl/it_it/policies/technologies/types/))
- Facebook ([informativa](https://www.facebook.com/help/cookies/))

## COME POSSO GESTIRE I COOKIE ALL'INTERNO DEL MIO BROWSER?

In aggiunta a quanto indicato in questo documento, l'Utente può gestire le preferenze relative ai Cookie direttamente all'interno del proprio browser ed impedire – ad esempio – che terze parti possano installarne. Tramite le preferenze del browser è inoltre possibile eliminare i Cookie installati in passato, incluso il Cookie in cui venga eventualmente salvato il consenso all'installazione di Cookie da parte di questo sito. È importante notare che disabilitando tutti i Cookie, il funzionamento di questo sito potrebbe essere compromesso. Puoi trovare informazioni su come gestire i Cookie nel tuo browser ai seguenti indirizzi: [Google Chrome](https://support.google.com/chrome/answer/95647?hl=en&p=cpn_cookies), [Mozilla Firefox](https://support.mozilla.org/it/kb/Attivare%2520e%2520disattivare%2520i%2520cookie?redirectlocale=en-US&redirectslug=Enabling+and+disabling+cookies), [Apple Safari](https://support.apple.com/kb/PH19214?viewlocale=it_IT&locale=en_US) e [Microsoft Windows Explorer](windows.microsoft.com/it-it/windows-vista/block-or-allow-cookies).

### Microsoft Internet Explorer

1. clicca su "Strumenti" nella parte superiore della finestra del browser;
2. seleziona "Opzioni Internet";
3. clicca sulla scheda "Privacy";
4. per attivare i cookies, il livello di Privacy deve essere impostato su "Medio" o al di sotto; impostando il livello di Privacy sopra il "Medio" l’utilizzo dei cookies verrà disattivato.

### Mozilla Firefox

1. clicca su "Strumenti" nella parte superiore della finestra del browser;
2. seleziona "Opzioni";
3. seleziona l'icona "Privacy";
4. clicca su "Cookies";
5. seleziona o meno le voci "Accetta i cookies dai siti" e "Accetta i cookies di terze parti";

### Google Chrome

1. clicca l'icona del menu;
2. seleziona "Impostazioni";
3. nella parte inferiore della pagina, seleziona "Mostra impostazioni avanzate";
4. nella sezione "Privacy", seleziona "Impostazioni contenuti";
5. seleziona o meno la voce "Impedisci ai siti di impostare dati".

### Apple Safari

1. clicca sull'etichetta "Safari" nella parte superiore della finestra del browser;
2. seleziona l'opzione "Preferenze";
3. clicca su "Privacy";
4. imposta la tua scelta alla voce "Cookie e dati di siti web".

Per maggiori informazioni sull’utilizzo dei cookies e su come bloccarli, è possibile visitare i seguenti siti: [www.youronlinechoices.eu](), [ www.allaboutcookies.org ]().

Le pubblicità sul nostro sito sono fornite anche da una organizzazione terza. I nostri partner pubblicitari forniranno annunci pubblicitari ritenuti di vostro interesse, sulla base delle informazioni raccolte a seguito delle vostre visite a questo e ad altri siti internet (le sopracitate informazioni sono raccolte in forma anonima e non includono il vostro nome, indirizzo, indirizzo di posta elettronica o numero di telefono). Al fine di operare tale raccolta anonima di dati il nostro partner potrebbe avere necessità di collocare un cookie (un breve file di testo) sul vostro computer. Per maggiori informazioni riguardo a questo tipo di pubblicità basata sui gusti degli utenti, sui cookies, e sulle modalità per esprimere il dissenso potete visitare il sito www.youronlinechoices.com


## TITOLARE DEL TRATTAMENTO DEI DATI

{{LEGAL_NAME}}

Dal momento che l'installazione di Cookie e di altri sistemi di tracciamento operata da terze parti tramite i servizi utilizzati all'interno di questa Applicazione non può essere tecnicamente controllata dal Titolare, ogni riferimento specifico a Cookie e sistemi di tracciamento installati da terze parti è da considerarsi indicativo. Per ottenere informazioni complete, consulta la privacy policy degli eventuali servizi terzi elencati in questo documento.

Vista l'oggettiva complessità legata all'identificazione delle tecnologie basate sui Cookie ed alla loro integrazione molto stretta con il funzionamento del web, l'Utente è invitato a contattare il Titolare qualora volesse ricevere qualunque approfondimento relativo all'utilizzo dei Cookie stessi e ad eventuali utilizzi degli stessi – ad esempio ad opera di terzi – effettuati tramite questo sito.
