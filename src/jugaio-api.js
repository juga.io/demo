'use strict'

const request = require("request")

function get(url, cb){
    // console.log("Calling "+url)
    return request(url, (err, res, body) => {
        if (err)
            return cb(err)
        try {
            cb(null, JSON.parse(body))
        } catch (e){
            cb(e)
        }
    })
}

function init(config){
    config = config || {}
    config.endpoint = config.endpoint || "https://api.juga.io"
    config.userId = config.userId

    return {
        readFeed: function(id, cb){
            get(`${config.endpoint}/api/1.0/feed/${id}?future=false`, (err, json) => {
                if (err)
                    return cb(err)
                cb(null, json.map((entry) => {
                    entry.date = new Date(entry.date)
                    return entry
                }))
            })
        }
    }
}

module.exports = init
