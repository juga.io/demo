'use strict';

/**
 * Ready to listen express app configured with all the routes and datastore.
 * @module app
 */

const path = require("path")
const config = require("./config")

// Initialize datastore refresh
require("./datastore").init()

// Create Express app
const express = require("express")
const app = express()

// Setup template engine
app.set("view engine", "pug")

// Sass + Bootstrap support
const sass = require("node-sass-middleware")
app.use(sass({
    src: path.join(__dirname, "..", "sass"),
    dest: path.join(__dirname, "..", "public", "css"),
    debug: config.production == false,
    outputStyle: "compressed",
    prefix: "/css"
}))
app.use("/css", express.static(path.join(__dirname, "..", "public", "css")))

// Serve bootstrap static files from node_modules
app.use("/components/bootstrap", express.static(path.join(__dirname, "..", "node_modules", "bootstrap-sass", "assets")))
app.use("/components/jquery", express.static(path.join(__dirname, "..", "node_modules", "jquery", "dist")))
app.use("/components/snapsvg", express.static(path.join(__dirname, "..", "node_modules", "snapsvg", "dist")))
app.use("/components/velocityjs", express.static(path.join(__dirname, "..", "node_modules", "velocity-animate")))
app.use("/components/hammerjs", express.static(path.join(__dirname, "..", "node_modules", "hammerjs")))

// Serve static files from public folder
app.use(express.static(path.join(__dirname, "..", "public")))

// Install Routes
app.use(require("./routes"))

// Schedule cronjob
if (config.publishSchedule){
    const publish = require("./publish")
    const schedule = require('node-schedule')
    schedule.scheduleJob(config.publishSchedule, publish)
}


module.exports = app
