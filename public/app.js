//
// Browser js
//

var App = {
    pageView: function(){
        gtag("event", "page_view");
        // TODO: ads refresh
    },
    preroll: function(){
    },
    intersticial: function(){
    },
    init: function(){
        App.pageView();
    }
};

JugaIoConfig = {
    onEvent: function(kind){
        if (kind == "PageView"){
            App.pageView();
            var menu = $(".navbar-header").height();
            var t = $(".juga-io").offset().top;
            $("html, body").animate({ scrollTop: (t - menu) + "px" });
        } else if (kind == "Action.OpenQuiz.PassQuestion"){
            App.preroll();
        } else if (kind == "Action.Game.Start" || kind == "Action.Game.Over"){
            App.intersticial();
        }
    }
};
