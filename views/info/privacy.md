# Privacy

Il presente documento ha lo scopo di descrivere le modalità di gestione del sito {{LEGAL_URL}} (di seguito per brevità {{LEGAL_NAME}}), in riferimento al trattamento dei dati personali degli utenti/visitatori che lo consultano.

Si tratta di un'informativa che è resa ai sensi dell'art. 13 del D. Lgs 196/03 - Codice in materia di protezione dei dati personali - a tutti gli utenti che si collegano al sito {{LEGAL_NAME}} all'indirizzo {{LEGAL_URL}} al fine di rendere edotti gli stessi in ordine alle modalità e finalità di trattamento dei loro dati personali.

 L'informativa è resa soltanto per il sito di {{LEGAL_NAME}} {{LEGAL_URL}} e non anche per altri siti web eventualmente consultati dall'utente tramite appositi link.  Gli utenti/visitatori dovranno leggere attentamente la presente Privacy Policy prima di inoltrare qualsiasi tipo di informazione personale e/o compilare qualunque modulo elettronico presente sul sito stesso.

##  Tipologia di dati trattati e finalità di trattamento

### 1. Dati di navigazione

I sistemi informatici e le procedure software preposte al funzionamento del sito web di {{LEGAL_NAME}} acquisiscono, nel corso del loro normale esercizio, alcuni dati personali la cui trasmissione è implicita nell'uso dei protocolli di comunicazione di Internet.

Si tratta di informazioni che non sono raccolte per essere associate a interessati identificati, ma che per loro stessa natura potrebbero, attraverso elaborazioni ed associazioni con dati detenuti da terzi, permettere di identificare gli utenti.  In questa categoria di dati rientrano gli indirizzi IP o i nomi a dominio dei computer utilizzati dagli utenti che si connettono al sito, gli indirizzi in notazione URI (Uniform Resource Identifier) delle risorse richieste, l'orario della richiesta, il metodo utilizzato nel sottoporre la richiesta al server, la dimensione del file ottenuto in risposta, il codice numerico indicante lo stato della risposta data dal server (buon fine, errore, ecc.) ed altri parametri relativi al sistema Operativo e all'ambiente informatico dell'utente.

Questi dati vengono utilizzati al solo fine di ricavare informazioni statistiche anonime sull'uso del sito e per controllarne il corretto funzionamento. I dati potrebbero essere utilizzati per l'accertamento di responsabilità in caso di ipotetici reati informatici ai danni del sito. 

### 2. Dati forniti volontariamente dall'utente

L'invio facoltativo, esplicito e volontario di posta elettronica agli indirizzi indicati su questo sito comporta la successiva acquisizione dell'indirizzo del mittente, necessario per rispondere alle richieste, nonché degli eventuali altri dati personali inseriti nella missiva. La volontaria registrazione al sito {{LEGAL_NAME}} {{LEGAL_URL}}, ove richiesta per l'ottenimento di particolari servizi, sarà preceduta da specifiche informative che verranno progressivamente riportate e visualizzate nelle apposite pagine del sito.

### 3. Cookies

Un "cookie" è una piccola quantità di dati contenenti un codice identificativo unico anonimo che vengono inviati al tuo browser da un server Web e che vengono successivamente memorizzati sul disco fisso del computer dell'utente. Il cookie viene poi riletto e riconosciuto solo dal sito Web che lo ha inviato ogni qualvolta si effettuino connessioni successive. Se l'utente preferisce non ricevere cookies, può impostare il proprio browser in modo tale che lo avverta della presenza di un cookie potendo così decidere se accettarlo o meno. Si possono anche rifiutare automaticamente tutti i cookies, attivando l'apposita opzione nel browser.

Nel sito di {{LEGAL_NAME}} i cookies vengono associati alla tua macchina e non identificano l’utente finale; peraltro, alcuni cookies, memorizzati temporaneamente sino al termine della tua navigazione, possono essere associati alla tua utenza, se sei un utente registrato. L’utilizzo dei cookies ha l’obiettivo di facilitare, personalizzare e velocizzare la tua esperienza di navigazione sul nostro sito, di controllare l’accesso ai servizi ai quali ti sei registrato e di ricavare statistiche anonime sugli accessi al sito. Se decidi di disabilitare tali cookies perderai buona parte delle funzionalità di personalizzazione ed alcuni servizi potrebbero non essere utilizzabili.

Alcune aziende forniscono inserzioni pubblicitarie sul sito di {{LEGAL_NAME}}. Tali aziende possono usare i cookies per accertarsi che non ti vengano visualizzate le stesse inserzioni troppo frequentemente o a scopo statistico per ricavarne i dati di traffico al fine di migliorarne i contenuti pubblicitari. Questi cookies vengono letti dalle aziende in modo anonimo, senza cioè procedere alla tua concreta identificazione, e sono gestiti direttamente dalle aziende stesse, che sono autonomi titolari del trattamento, e {{LEGAL_NAME}} non ha accesso ad essi, né può controllare il modo in cui vengono successivamente utilizzati.

##  Modalità di trattamento dei dati personali

I dati personali sono trattati con strumenti automatizzati per il tempo strettamente necessario a conseguire gli scopi per cui sono stati raccolti.  Specifiche misure di sicurezza sono osservate per prevenire la perdita dei dati, usi illeciti o non corretti ed accessi non autorizzati.

I dati da Lei comunicati al momento della registrazione al sito {{LEGAL_URL}}, che permetteranno di farLa partecipare alle iniziative di {{LEGAL_NAME}} e di offrirLe i servizi previsti da questo sito internet, saranno inseriti in un archivio anagrafico clienti al momento della prima registrazione sul Sito, con digitazione di un codice identificativo ed una password che dovrà essere da Lei conservata ed utilizzata per le successive operazioni.

 I dati forniti in sede di registrazione al sito saranno trattati direttamente da {{LEGAL_NAME}} per le seguenti finalità:

- per l’elaborazione e trasmissione delle comunicazioni previste dalle condizioni generali del servizio;
- per l’invio di fatture commerciali. 

Il conferimento dei dati è facoltativo. Tuttavia, senza i Suoi dati, ed in particolare senza che Lei ci fornisca i dati di cui ai campi contraddistinti dalla dicitura "Dato Obbligatorio", non potremo consentirLe di accedere ai nostri servizi e gli stessi non potranno pertanto esserLe forniti.  

I dati personali che Lei ci fornirà verranno registrati e custoditi su supporti informatici protetti e trattati nel pieno rispetto delle misure di sicurezza idonee a garantire la tutela della Sua riservatezza. I Suoi dati potranno essere comunicati a tutte le Società del gruppo nonchè alle società dalla stessa partecipate o ad essa collegate ed a quelle società che svolgono per nostro conto compiti di natura tecnica od organizzativa funzionali alla fornitura dei servizi richiesti.

## Comunicazione e diffusione 

I soggetti o le categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza dei dati sono per la gestione tecnica del servizio, Amazon anagrafica (servizio host per sito) e gestione operativa {{LEGAL_NAME}} (servizio content feeding), soggetti esterni, formalmente incaricati (art.29).

## I suoi dati personali non sono oggetto di diffusione.

Diritto degli interessati Ha il diritto di farli aggiornare, integrare, rettificare, modificare o cancellare gratuitamente, chiederne il blocco ed opporsi al loro trattamento, così come previsto dall.art.7 del D.Lgs 196/2003. Potrà esercitare senza ritardo i diritti previsti comunicando la sua volontà al Responsabile del Trattamento c/o {{LEGAL_NAME}}, - E-mail. info at {{LEGAL_DOMAIN}}.
