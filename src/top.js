/**
 * This module is responsible of computing most visited URLs for the website.
 * @module top
 */
'use strict';

const moment = require('moment');
const googleapis = require('googleapis')
const analytics = googleapis.analytics('v3')
const JWT = googleapis.auth.JWT
const config = require('./config')

/*
 * Generates a report of most visited URL.
 * @param {JSON} key - the service .json file provided by google in the developer console
 * @param {string} analyticsViewId - The google analytics view id
 * @param {string} startDate - Report start YYYY-MM-DD format
 * @param {string} endDate - Report end YYYY-MM-DD format
 * @return {Promise.<Array<UrlStat>>}
 */
function googleAnalyticsTopPageViewsReport(key, analyticsViewId, startDate, endDate){
    return new Promise((fulfill, reject) => {
        let auth = new JWT(
            key.client_email,
            null,
            key.private_key,
            ['https://www.googleapis.com/auth/analytics.readonly']
        );
        auth.authorize((err, tokens) => {
            if (err)
                return reject(err)
            analytics.data.ga.get({
                'auth': auth,
                'ids': 'ga:'+analyticsViewId,
                'start-date': startDate,
                'end-date': endDate,
                'metrics': 'ga:uniquePageviews',
                'dimensions': 'ga:pagePath',
                'sort': '-ga:uniquePageviews'
            }, (err, result) => {
                if (err)
                    return reject(err)
                if (result.rows == null){
                    console.log("Google analytics rows is not defined, empty report or bad configuration: "+JSON.stringify(result))
                    return fulfill([])
                }
                return fulfill(result.rows.map(arr => UrlStat(arr[0], arr[1])))
            });
        });
    })
}


/**
 * @typedef UrlStat
 * @property {string} url - The URL
 * @property {int} views - The number of page views
 */
function UrlStat(url, views){
    return { url:url, views: views }
}

/**
 * Returns sorted array of most visited URLs and their pageview counter.
 * @return {Promise<Array<UrlStat>>}
 */
function getTopUrls(){
    if (config.analyticsServiceKey)
        return googleAnalyticsTopPageViewsReport(
            config.analyticsServiceKey,
            config.analyticsViewId,
            moment().subtract(30, "days").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD")
        )
    else
        return Promise.resolve([]);
}

/**
 * Returns most visited items ids.
 * @return {Promise<Array<string>>}
 */
function getTopItemIds(){
    const reg = /^\/item\/(.*)\.html$/
    return getTopUrls().then((urls) => urls.
                      map(o => o.url.match(reg)).
                      filter(o => o != null).
                      map(o => o[1]))
}

module.exports.getTopUrls = getTopUrls
module.exports.getTopItemIds = getTopItemIds
