FROM node:9.4.0

# install httrack & aws client
RUN apt-get update
RUN apt-get install -y httrack python-dev python-pip && pip install awscli
RUN rm -rf /var/lib/apt/lists/*

# install dependencies
COPY package.json app/
RUN cd app && npm install

# install sources
COPY bin app/bin
COPY public app/public
COPY sass app/sass
COPY src app/src
COPY tests app/tests
COPY views app/views

EXPOSE 8080
WORKDIR app/
CMD ["yarn", "start"]
