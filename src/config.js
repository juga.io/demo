function readJsonFile(path) {
  if (path == null) return null;
  return JSON.parse(require("fs").readFileSync(path));
}

const config = {
  version: 2,
  itemsPerPage: 10,
  topItems: 10,
  locale: "en",
  production: process.env.PRODUCTION != null || false,
  debug: process.env.DEBUG != null || false,
  localDevelop: process.env.LOCAL_DEV != null || false,
  googleAnalyticsId: process.env.GOOGLE_ANALYTICS_ID || false,
  fbAppId: process.env.FACEBOOK_APP_ID || false,
  fbPagesId: process.env.FACEBOOK_PAGES_ID || false,
  addthisProfileId: process.env.ADDTHIS_ID || false,
  oneSignalAppId:
    process.env.ONE_SIGNAL_APP_ID || "ad964053-7a46-4505-b444-04aca9f39551",
  siteName: "Demo juga.io",
  contactEmail: "info@juga.io",
  title: "Juga.io platform demo website",
  description: "Juga.io platform demo website",
  domain: "demo.juga.io",
  stagingUrl: "https://demo.juga.io",
  url: "http://localhost:5000",
  shareImage: "http://localhost:5000/share.png",
  legal: {
    name: "Juga.io",
    url: "https://demo.juga.io",
    domain: "demo.juga.io"
  },
  jugaIo: {
    refreshInterval: 10000,
    userId: "5f53a3ce-aadf-4df8-87fd-dac1ac13d48d",
    feedId: "c50c6d81-98a4-477f-b6fc-f6c4426e097b",
    endpoint: "https://api.juga.io",
    jsUrl: "https://cdn.juga.io/jugaio.js",
    jsDepsUrl: "https://cdn.juga.io/jugaio-client-deps.js",
    css: "https://cdn.juga.io/css/jugaio.css"
  },
  adminPassword: process.env.ADMIN_PASSWORD || "password",
  s3MaxAge: 1800,
  s3Bucket: "io.juga.demo.www",
  s3Region: "eu-central-1",
  publishSchedule: null,
  analyticsViewId: process.env.GOOGLE_ANALYTICS_VIEW_ID,
  analyticsServiceKey: readJsonFile(process.env.GOOGLE_ANALYTICS_SERVICE_KEY),
  refreshTopInterval: 12 * 60 * 60 * 1000,
  categories: [
    { key: "Slideshow", title: "Slideshow" },
    { key: "VersusWar", title: "Versus war" },
    { key: "OpenQuiz", title: "Open quiz" },
    { key: "Quiz", title: "Quiz" },
    { key: "Test", title: "Personality test" },
    { key: "HotOrNot", title: "Hot Or Not" },
    { key: "Calculator", title: "Calculators" },
    { key: "Article", title: "Article" },
    { key: "Tournament", title: "Tournament" }
  ]
};

if (config.production) {
  config.url = "https://demo.juga.io";
  config.shareImage = "https://demo.juga.io/share.png";
  // config.publishSchedule = null
}

module.exports = config;
