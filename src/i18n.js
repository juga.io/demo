const en = {
    newer: "Newer",
    older: "Older",
    topItems: "Top content",
    readMore: "Read more…"
}
const it = {
    readMore: "Leggi…",
    newer: "Più Nuovo",
    older: "Più vecchio"
}

module.exports = Object.assign({}, en, it)
