/**
 * Contains website categories
 * @module categories
 */


const config = require('./config');
const categories = {};

/**
 * @typedef Category
 * @property {int} priority - Menu priority
 * @property {string} key - Short id
 * @property {string} title - The category title
 * @property {function} match - A filtering function which returns true when a FeedEntry is member of this category
 */
function createCategory(priority, key, title, match){
    categories[key] = {
        priority: priority,
        key: key,
        title: title,
        match: (match != null ? match : (item) => item.kind == key)
    }
}

/**
 * Returns Category
 * @param {string} key - Category ID
 * @return {Category}
 * @static
 */
function find(key){
    return categories[key]
}

/**
 * Returns Categories sorted by priority.
 * @return {Array.<Category>}
 * @static
 */
function sorted(){
    return Object.values(categories)
        .sort((a,b) => a.priority > b.priority)
}

// populate categories using config
config.categories.forEach((catDef, idx) => {
  createCategory(idx + 1, catDef.key, catDef.title);
})

module.exports.find = find
module.exports.sorted = sorted()
